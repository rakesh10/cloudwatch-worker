from flask import Flask, request, jsonify
from pystatsd import Client, Server
import psutil


app = Flask(__name__)

import math

"""
@author: Rakesh Bhatt 

1, Script to send cloudwatch data to server
2. Prepare final script to deliver stats to cloudwatch usin statsd
3. Current port of statsd deamon is default 8125/change if you need have another port
4. Script will be calling push_matrix api and it will trigger statsd calls

"""

@app.route("/")
def index():
    # print ": This is my index : "
    return jsonify({"msg": "System Monitor is On"}),  200

def convert_size(size_bytes):
   # return size_bytes
   if size_bytes == 0:
       return 0
   else:
       return float(float(size_bytes)/(1024*1024*1024))

@app.route("/push_metrix")
def push_metrix():

    statsd = Client('localhost', 8125)

    cpu_dict = {}

    cpu_times = psutil.cpu_times()
    for name, value in cpu_times._asdict().iteritems():
        cpu_dict[name] = value

    cpu_percent = psutil.cpu_percent()
    # print "CPU Percent : ", cpu_percent
    cpu_dict["percent"] = cpu_percent
    virtual_memory = psutil.virtual_memory()
    # print "Virtual memory : ", virtual_memory

    memory_dict = {}
    for name, value in virtual_memory._asdict().iteritems():
        avail_memory = value
        if name != "percent":
            avail_memory = convert_size(value)

        memory_dict[name] = avail_memory

    swap_memory = psutil.swap_memory()
    swap_memory_dict = {}

    for name, value in swap_memory._asdict().iteritems():
        swap_memory_dict[name] = value

    partions = psutil.disk_partitions()
    partion_details = []

    for partition in partions:
        partition_detail = {}
        for name, value in partition._asdict().iteritems():
            partition_detail[name] = value

        partion_details.append(partition_detail)

    partions = []

    for partition in partion_details:
        print "Partition : ", partition.get("device")
        partition_stats = {}

        for name, value in psutil.disk_usage(partition.get("mountpoint"))._asdict().iteritems():
            if name != "percent":
                avail_memory = convert_size(value)
            else:
                avail_memory = value

            partition_stats[name] = avail_memory

        partition_stats["mount"] = partition.get("mountpoint");

        partions.append(partition_stats)

    def get_max_stats(max_stats, mem_stats):
        if max_stats.get("per") < mem_stats.get("percent"):
            max_stats["per"] = mem_stats["percent"]
            max_stats["total"] = mem_stats["total"]
            max_stats["used"] = mem_stats["used"]

        return max_stats

    partions = sorted(partions, key=lambda k: k['total'], reverse=True)
    max_stats = reduce(lambda max_stats, mem_stats: get_max_stats(max_stats, mem_stats), partions[:2], {"total": 0, "per": 0, "used": 0})

    # needed matrix is
    """
    Total used matrix 
    
    cpu_percent
    max_consumed_drive_usage_percent
    max_consumed_total_spaces
    mamory_percent
    
    """

    needed_matrix = {
        "cpu_percent": cpu_dict.get("percent"),
        "mamory_percent": memory_dict.get("percent"),
        "max_used": max_stats.get("used"),
        "max_percent": max_stats.get("per"),
        "max_total": max_stats.get("total")
    }

    statsd.gauge("memory.percent", needed_matrix.get("max_percent"))
    statsd.gauge("memory.used", needed_matrix.get("max_used"))
    statsd.gauge("memory.total", needed_matrix.get("max_total"))
    statsd.gauge("cpu.memory", needed_matrix.get("mamory_percent"))
    statsd.gauge("cpu.percent", needed_matrix.get("cpu_percent"))

    data =  {
        "cpu": cpu_dict,
        "memory": memory_dict,
        "swap": swap_memory_dict,
        "partion_stats": partions,
        "cloudwatch": needed_matrix
    }

    return jsonify(data)



app.dubug=True